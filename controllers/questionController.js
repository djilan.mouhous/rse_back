// Import question model
Question = require('../models/questionModel');
// Handle index actions
exports.index = function (req, res) {
    Question.get(function (err, questions) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else{
        res.json({
            status: "success",
            message: "Questions retrieved successfully",
            data: questions
        });}
    });
}
// Handle create question actions
exports.new = function (req, res) {
    var question = new Question();
    question.name = req.body.name;
    question.question = req.body.question;
    question.cat = req.body.cat;
    question.partie = req.body.partie;
    question.options = req.body.options;
    question.type = req.body.type;
    question.value = 0;
    question.coef = req.body.coef;
    question.minima = req.body.minima;
    // save the question and check for errors
    question.save(function (err) {
            
            if (!err)
            res.json({
                message: 'New question created!',
                data: question
            });
            else 
            res.json(err);
    });
};
// Handle view question info
exports.view = function (req, res) {
    Question.findById(req.params.question_id, function (err, question) {
        
        res.json({
            message: 'Question details loading..',
            data: question
        });
    });
}
// Handle update question info
exports.update = function(req, res) {
Question.findById(req.params.question_id, function (err, question) {
        if (err)
            res.send(err);
            question.name = req.body.name;
            question.question = req.body.question;
            question.cat = req.body.cat;
            question.partie = req.body.partie;
            question.type = req.body.type;
            question.options = req.body.options;
            question.value = 0;
            question.coef = req.body.coef;
            question.minima = req.body.minima;
// save the question and check for errors
        question.save(function (err) {
            res.json({
                message: 'Question Info updated',
                data: question
            });
        });
    });
}
// Handle delete question
exports.delete = function (req, res) {
    Question.remove({
        _id: req.params.question_id
    }, function (err, question) {
        // if (err)
        //     res.send(err);
        res.json({
            status: "success",
            message: 'Question deleted'
        });
    });
};
