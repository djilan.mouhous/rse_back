// Import partie model
Partie = require('../models/partieModel');
// Handle index actions
exports.index = function (req, res) {
    Partie.get(function (err, parties) {
        // if (err) {
        //     res.json({
        //         status: "error",
        //         message: err,
        //     });
        // }
        res.json({
            status: "success",
            message: "Parties retrieved successfully",
            data: parties
        });
    });
}
// Handle create partie actions
exports.new = function (req, res) {
    var partie = new Partie();
    partie.name = req.body.name;
    partie.id = req.body.id;
    partie.slug = req.body.slug;
    // save the partie and check for errors
    partie.save(function (err) {
        // if (err)
        //     res.json(err);
        res.json({
            message: 'New partie created!',
            data: partie
        });
    });
};

// Handle view partie info
exports.view = function (req, res) {
    Partie.findById(req.params.partie_id, function (err, partie) {
        // if (err)
        //     res.send(err);
        res.json({
            message: 'Partie details loading..',
            data: partie
        });
    });
}
// Handle update partie info
exports.update = function (req, res) {
    Partie.findById(req.params.partie_id, function (err, partie) {
        if (err)
            res.send(err);
            partie.name = req.body.name;
            partie.id = req.body.id;
            partie.slug = req.body.slug;
// save the partie and check for errors
        partie.save(function (err) {
            // if (err)
            //     res.json(err);
            res.json({
                message: 'Partie Info updated',
                data: partie
            });
        });
    });
}
// Handle delete partie
exports.delete = function (req, res) {
    Partie.remove({
        _id: req.params.partie_id
    }, function (err, partie) {
        // if (err)
        //     res.send(err);
        res.json({
            status: "success",
            message: 'Partie deleted'
        });
    });
};
