// Import category model
Category = require('../models/categoryModel');
// Handle index actions
exports.index = function (req, res) {
    Category.get(function (err, categorys) {
        // if (err) {
        //     res.json({
        //         status: "error",
        //         message: err,
        //     });
        // }
        res.json({
            status: "success",
            message: "Categorys retrieved successfully",
            data: categorys
        });
    });
}
// Handle create category actions
exports.new = function (req, res) {
    var category = new Category();
    category.name = req.body.name;
    category.id = req.body.id;
    category.slug = req.body.slug;
    category.gradient = req.body.gradient;
    category.min = req.body.min;
    category.value = req.body.value;
    // save the category and check for errors
    category.save(function (err) {
        // if (err)
        //     res.json(err);
        res.json({
            message: 'New category created!',
            data: category
        });
    });
};
// Handle view category info
exports.view = function (req, res) {
    Category.findById(req.params.category_id, function (err, category) {
        // if (err)
        //     res.send(err);
        res.json({
            message: 'Category details loading..',
            data: category
        });
    });
}
// Handle update category info
exports.update = function (req, res) {
    Category.findById(req.params.category_id, function (err, category) {
        if (err)
            res.send(err);
            category.name = req.body.name;
            category.id = req.body.id;
            category.slug = req.body.slug;
            category.gradient = req.body.gradient;
            category.min = req.body.min;
            category.value = req.body.value;
// save the category and check for errors
        category.save(function (err) {
            // if (err)
            //     res.json(err);
            res.json({
                message: 'Category Info updated',
                data: category
            });
        });
    });
}
// Handle delete category
exports.delete = function (req, res) {
    Category.remove({
        _id: req.params.category_id
    }, function (err, category) {
        // if (err)
        //     res.send(err);
        res.json({
            status: "success",
            message: 'Category deleted'
        });
    });
};
