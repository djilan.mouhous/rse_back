// Import express
let express = require('express');
// Import Body parser
let bodyParser = require('body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Import CORS
let cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();
// Initialise the app
let app = express();
console.log(process.env.MONGODB_URI)
const connectionString = process.env.MONGODB_URI

// Import routes
let apiRoutes = require("./api-routes");
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors())
// Connect to Mongoose and set connection variable
mongoose.connect(connectionString);
var db = mongoose.connection;

var corsOption = {
    origin : ["https://rseadmin.alizeegillouaye.com", "https://rse-label.djilan.fr"],
    optionsSuccessStatus: 200,
    methods : ["GET", "PUT", "DELETE", "POST"],
    //credentials:true
}
// Added check for DB connection
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

// Setup server port
var port = process.env.PORT || 8080;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Use Api routes in the App
app.use('/api',cors(corsOption), apiRoutes);
// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});