// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import question controller
var questionController = require('./controllers/questionController');
// Contact routes
router.route('/questions')
    .get(questionController.index)
    .post(questionController.new);

router.route('/questions/:question_id')
    .get(questionController.view)
    .patch(questionController.update)
    .put(questionController.update)
    .delete(questionController.delete);

// Import partie controller
var partieController = require('./controllers/partieController');
// Contact routes
router.route('/parties')
    .get(partieController.index)
    .post(partieController.new);

router.route('/parties/:partie_id')
    .get(partieController.view)
    .patch(partieController.update)
    .put(partieController.update)
    .delete(partieController.delete);

// Import category controller
var categoryController = require('./controllers/categoryController');
// Contact routes
router.route('/categorys')
    .get(categoryController.index)
    .post(categoryController.new);

router.route('/categorys/:category_id')
    .get(categoryController.view)
    .patch(categoryController.update)
    .put(categoryController.update)
    .delete(categoryController.delete);
// Export API routes
module.exports = router;