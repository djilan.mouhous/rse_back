var mongoose = require('mongoose');
// Setup schema
var categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    id: {
        type: Number,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    gradient: {
        type: String,
        required: false,
        default: "linear-gradient(45deg, rgb(96, 156, 97), rgb(103, 174, 234))"
    },
    min: {
        type: Number,
        required: true
    },
    value: {
        type: Number,
        required: false,
        default : 0
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Category model
var Category = module.exports = mongoose.model('category', categorySchema);

module.exports.get = function (callback, limit) {
    Category.find(callback).limit(limit);
}