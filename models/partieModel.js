var mongoose = require('mongoose');
// Setup schema
var partieSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    id: {
        type: Number,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
var Partie = module.exports = mongoose.model('partie', partieSchema);

// Export Partie model
module.exports.get = function (callback, limit) {
    Partie.find(callback).limit(limit);
}