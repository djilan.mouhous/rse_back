var mongoose = require('mongoose');
// Setup schema
var questionSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    question: {
        type: String,
        required: true
    },
    cat: {
        type: Number,
        required: true
    },
    partie: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    options: {
        type: Array,
        required: true
    },
    value: {
        type: Number,
        required: false,
        default: true
    },
    coef: {
        type: String,
        required: true
    },
    minima: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
var Question = module.exports = mongoose.model('question', questionSchema);

// Export Question model
module.exports.get = function (callback, limit) {
    Question.find(callback).limit(limit);
}